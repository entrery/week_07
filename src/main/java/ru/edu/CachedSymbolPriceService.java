package ru.edu;

import ru.edu.model.Symbol;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CachedSymbolPriceService implements SymbolPriceService {

    /**
     * delegate.
     */
    private final SymbolPriceService delegate;

    /**
     * cache.
     */
    private final Map<String, Symbol> cache =
            Collections.synchronizedMap(new HashMap<>());

    /**
     * not a magicNumber.
     */
    private static final int BLIN = 5;


    /**
     * constructor.
     *
     * @param price1
     */
    public CachedSymbolPriceService(final SymbolPriceService
                                            price1) {
        delegate = price1;
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого
     * данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return
     */
    @Override
    public Symbol getPrice(final String symbolName) {

        synchronized (symbolName) {


            if (!cache.containsKey(symbolName)
                    || Instant.now().minus(BLIN,
                                    ChronoUnit.SECONDS).
                            isAfter(cache.get(symbolName).
                                    getTimeStamp())) {
                cache.put(symbolName, delegate.getPrice(symbolName));
            }

        }
        return cache.get(symbolName);
    }
}
