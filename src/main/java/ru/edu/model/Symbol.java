package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена.
 */
public interface Symbol {

    /**
     * get.
     * @return symbol
     */
    String getSymbol();

    /**
     * get.
     * @return Price
     */
    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return TimeStamp
     */
    Instant getTimeStamp();
}
