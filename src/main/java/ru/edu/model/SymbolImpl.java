package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

public class SymbolImpl implements Symbol {

    /**
     * token name.
     */
    private String symbol;

    /**
     * token price.
     */
    private BigDecimal price;

    /**
     * time delay.
     */
    private Instant timestamp = Instant.now();

    /**
     * constructor.
     */
    public SymbolImpl() {
    }

    /**
     * constructor with arguments.
     *
     * @param price1
     * @param symbol1
     * @param timestamp1
     */
    public SymbolImpl(final String symbol1, final BigDecimal price1,
                      final Instant timestamp1) {
        this.symbol = symbol1;
        this.price = price1;
        this.timestamp = timestamp1;
    }

    /**
     * get token name.
     * @return symbol
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * get token price.
     * @return price
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * get time delay.
     * @return timestamp
     */
    @Override
    public Instant getTimeStamp() {
        return timestamp;
    }

    /**
     * set token name.
     * @param symbol1
     */
    public void setSymbol(final String symbol1) {
        this.symbol = symbol1;
    }

    /**
     * set token price.
     * @param price1
     */
    public void setPrice(final BigDecimal price1) {
        this.price = price1;
    }

    /**
     * set time delay.
     * @param timestamp1
     */
    public void setTimestamp(final Instant timestamp1) {
        this.timestamp = timestamp1;
    }

    /**
     * to string.
     * @return string
     */
    @Override
    public String toString() {
        return "SymbolImpl{"
                +
                "symbol='" + symbol + '\''
                +
                ", price=" + price
                +
                ", timestamp=" + timestamp
                +
                '}';
    }


}
