package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;


public class Watcher implements Runnable {

    /**
     * logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Watcher.class);

    /**
     * token to watch.
     */
    private final String symbolToWatch;

    /**
     * watched token price.
     */
    private final SymbolPriceService service;

    /**
     * previous value.
     */
    private Symbol lastDate;

    /**
     * Time to wait.
     */
    private static final long MILLIS = 1_000L;

    /**
     * constructor with arguments.
     *
     * @param symbol1
     * @param service1
     */
    public Watcher(final String symbol1, final SymbolPriceService service1) {
        this.symbolToWatch = symbol1;
        this.service = service1;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        Symbol symbol = service.getPrice(symbolToWatch);
        LOGGER.info("Получили инфо: {}", symbol, lastDate);
        getWriter(symbol);
        lastDate = symbol;

        try {

            Thread.sleep(MILLIS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * writer.
     *
     * @param symbol
     */
    private void getWriter(final Symbol symbol) {
        String fileName = "src/test/java/ru/edu/file.txt";
        try
                (FileWriter writer = new FileWriter(fileName, true)) {
            String str;
            if (lastDate == null) {
                str = "0";
            } else {
                str = String.valueOf(lastDate.getPrice().
                        subtract(symbol.getPrice()));
            }

            writer.write(LocalDate.now() + ", Price: " + symbol.getPrice()
                    + "" + str + "\n");

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
