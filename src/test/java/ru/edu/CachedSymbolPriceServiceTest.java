package ru.edu;

import org.junit.Test;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;


import static org.junit.Assert.*;

public class CachedSymbolPriceServiceTest {

    SymbolPriceService service = new BinanceSymbolPriceService();

    SymbolPriceService priceService = new CachedSymbolPriceService(service);

    public static final String SYMBOL_NAME = "BTCUSDT";


    @Test
    public void getPrice() throws InterruptedException {
        Symbol one = priceService.getPrice(SYMBOL_NAME);
        Symbol two = priceService.getPrice(SYMBOL_NAME);
        assertEquals(one.getPrice(),two.getPrice());



    }
}
