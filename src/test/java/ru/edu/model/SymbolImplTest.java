package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.Assert.*;

public class SymbolImplTest {

    SymbolImpl symbol = new SymbolImpl("BTCUSDT", new BigDecimal("0.1644864644"), Instant.now());

    @Before


    @Test
    public void getSymbol() {
        assertEquals("BTCUSDT", symbol.getSymbol());
    }

    @Test
    public void getPrice() {
        assertEquals(new BigDecimal("0.1644864644"), symbol.getPrice());
    }

    @Test
    public void getTimestamp() {
        assertNotNull(symbol.getTimeStamp());
    }

    @Test
    public void setSymbol() {
        symbol.setSymbol("Boris");
        assertEquals("Boris", symbol.getSymbol());
    }

    @Test
    public void setPrice() {
        symbol.setPrice(new BigDecimal("1.015"));
        assertEquals(new BigDecimal("1.015"), symbol.getPrice());
    }

    @Test
    public void setTimestamp() {
        symbol.setTimestamp(Instant.now());
        assertNotNull(symbol.getTimeStamp());

    }

    @Test
    public void testToString() {
        assertNotNull(symbol.toString());
    }


}