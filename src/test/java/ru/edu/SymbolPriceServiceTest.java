package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;
import ru.edu.model.Symbol;

import java.sql.SQLOutput;
import java.time.Instant;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SymbolPriceServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SymbolPriceServiceTest.class);

    public static final String SYMBOL_NAME = "BTCUSDT";

    private SymbolPriceService service;

    private Symbol mockSymbol = mock(Symbol.class);

    @Test
    public void getPrice() throws InterruptedException {

        /**
         * примерный тест проверки работы кэша, делаем 2 вызова и смотрим что данные были получены в одно время
         * после ожидания таймаута жизни данных в кэше делаем повторный запрос и проверяем, что данные имеют метку времени,
         * полученную после сброса.
         */
        service = mock(SymbolPriceService.class);


        when(service.getPrice(SYMBOL_NAME)).thenReturn(mockSymbol);
        when(mockSymbol.getTimeStamp()).thenReturn(Instant.now());

        Symbol price = service.getPrice(SYMBOL_NAME);
        Symbol priceAgain = service.getPrice(SYMBOL_NAME);
        assertEquals(price.getTimeStamp(), priceAgain.getTimeStamp());

        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("Сообщение отладки: {}", price);
        }

        LOGGER.debug("Сообщение отладки: {}", price);

        long timeout = 11_000L;
        LOGGER.info("Ждем сброса кэша: {} сек ",timeout);
        try {
            Thread.sleep(timeout);
            if(true) throw new RuntimeException(" My Exception");
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
        Symbol priceNew = service.getPrice(SYMBOL_NAME);
        assertFalse(price.getTimeStamp().isBefore(priceNew.getTimeStamp()));
    }
}