package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class WatcherTest {

    SymbolPriceService service = new BinanceSymbolPriceService();
    SymbolPriceService priceService = new CachedSymbolPriceService(service);

    Watcher watcher;

    private final static String TOKEN = "BTCUSDT";



    @Test
    public void run() throws InterruptedException {
        watcher = new Watcher(TOKEN, priceService);
        for(int i = 0; i < 15; i++)
            watcher.run();
            Thread.sleep(1_000L);

    }
}